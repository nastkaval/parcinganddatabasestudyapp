//
//  PhotoCollectionCell.swift
//  ParcingAndDatabaseStudyApp(Alamofire+REALM)
//
//  Created by Nastassia  Kavalchuk on 05.06.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit

class PhotoCollectionCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var imageFromUrl: UIImageView! 
}
