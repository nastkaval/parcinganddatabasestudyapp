//
//  CommentCell.swift
//  ParcingAndDatabaseStudyApp(Alamofire+REALM)
//
//  Created by Nastassia  Kavalchuk on 05.06.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
}
