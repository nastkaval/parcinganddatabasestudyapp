//
//  UserCell.swift
//  TravelApp
//
//  Created by Nastassia  Kavalchuk on 26.05.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var addressStreetLabel: UILabel!
    @IBOutlet weak var addressSuiteLabel: UILabel!
    @IBOutlet weak var addressCityLabel: UILabel!
    @IBOutlet weak var addressZipcodeLabel: UILabel!
    @IBOutlet weak var geoLatLabel: UILabel!
    @IBOutlet weak var geoLngLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var companyCatchpraseLabel: UILabel!
    @IBOutlet weak var companyBsLabel: UILabel!
}
