//
//  TodosViewController.swift
//  ParcingAndDatabaseStudyApp(Alamofire+REALM)
//
//  Created by Nastassia  Kavalchuk on 04.06.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit

class TodosViewController: UIViewController {
    
    // MARK: - Properties
    var todos: [Todo] = []
    
    // MARK: - Outlets
    @IBOutlet weak var todosTableView: UITableView!
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        ApiManagerTodo.instance.getTodos()
        todos = DataBaseManager.instance.getAllFromDatabase(Todo.self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.todosTableView.reloadData()
    }
}

     // MARK: - Extensions
extension TodosViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = todosTableView.dequeueReusableCell(withIdentifier: "TodoCell", for: indexPath) as! TodoCell
        let todo = todos[indexPath.row]
        cell.titleLabel.text = todo.title
        return cell
    }
}
