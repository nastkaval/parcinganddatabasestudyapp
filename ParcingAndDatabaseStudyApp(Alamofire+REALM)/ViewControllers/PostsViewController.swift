//
//  ViewController.swift
//  ParcingAndDatabaseStudyApp(Alamofire+REALM)
//
//  Created by Nastassia  Kavalchuk on 30.05.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit
import Foundation

class PostsViewController: UIViewController {
    
    // MARK: - Properties
    var posts: [Post] = []
    
    // MARK: - Outlets
    @IBOutlet weak var postsTableView: UITableView!
   
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        ApiManagerPost.instance.getPosts()
        posts =  DataBaseManager.instance.getAllFromDatabase(Post.self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.postsTableView.reloadData()
    }
}

    // MARK: - Extensions
extension PostsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = postsTableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
        if posts.count > 0 {
            let post = posts[indexPath.row]
            cell.titleLabel.text = post.title
            cell.bodyLabl.text = post.body
        }
        return cell
    }
}

