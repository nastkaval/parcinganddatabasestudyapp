//
//  UsersListViewController.swift
//  TravelApp
//
//  Created by Nastassia  Kavalchuk on 17.05.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit
import Foundation
import RealmSwift

class UsersListViewController: UIViewController {
    
    // MARK: - Properties
    var users : [User] = []

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        ApiManagerUser.instance.getUsers()
        users = DataBaseManager.instance.getAllFromDatabase(User.self)
    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
}

    // MARK: - Extensions
extension UsersListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
        if users.count > 0 {
            let user = users[indexPath.row]
            cell.nameLabel.text = user.name
            cell.usernameLabel.text = user.username
            cell.phoneLabel.text = user.phone
            cell.emailLabel.text = user.email
            cell.websiteLabel.text = user.website
            cell.addressCityLabel.text = user.address?.city
            cell.addressSuiteLabel.text = user.address?.suite
            cell.addressStreetLabel.text = user.address?.street
            cell.addressZipcodeLabel.text = user.address?.zipcode
            cell.geoLatLabel.text = user.address?.geo?.latitude
            cell.geoLngLabel.text = user.address?.geo?.longitude
            cell.companyBsLabel.text = user.company?.bs
            cell.companyNameLabel.text = user.company?.name
            cell.companyCatchpraseLabel.text = user.company?.catchPhrase
        }
        return cell
    }
}

