//
//  CommentsViewController.swift
//  ParcingAndDatabaseStudyApp(Alamofire+REALM)
//
//  Created by Nastassia  Kavalchuk on 04.06.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit

class CommentsViewController: UIViewController {
    
    // MARK: - Properties
    var comments: [Comment] = []
   
    // MARK: - Outlets
    @IBOutlet weak var commentsTableView: UITableView!
   
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        ApiManagerComment.instance.getComments()
        comments = DataBaseManager.instance.getAllFromDatabase(Comment.self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.commentsTableView.reloadData()
    }

}

     // MARK: - Extensions
extension CommentsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = commentsTableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
        let comment = comments[indexPath.row]
        cell.titleLabel.text = comment.name
        cell.emailLabel.text = comment.email
        cell.bodyLabel.text = comment.body
        return cell
    }
}
