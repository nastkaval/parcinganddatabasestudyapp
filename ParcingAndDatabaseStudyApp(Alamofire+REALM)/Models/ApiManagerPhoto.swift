//
//  ApiManagerPhoto.swift
//  ParcingAndDatabaseStudyApp(Alamofire+REALM)
//
//  Created by Nastassia  Kavalchuk on 31.05.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import Foundation
import Alamofire

class ApiManagerPhoto {
    static var instance = ApiManagerPhoto()
    private enum Constants {
        static let baseURL = "https://jsonplaceholder.typicode.com"
    }
    private enum EndPoints {
        static let photos = "/photos"
    }
    
    func getPhotos() {
        let urlString = Constants.baseURL + EndPoints.photos
        Alamofire.request(urlString, method: .get, parameters: [:]).responseJSON { (response) in
            if let arrayPhotos = response.result.value as? Array< Dictionary<String,Any> >{
                var photos: [Photo] = []
                for photoDict in arrayPhotos {
                    let photo = Photo()
                    photo.albumId = "\(photoDict["userId"] as? Int ?? 0)"
                    photo.id = "\(photoDict["id"] as? Int ?? 0)"
                    photo.title = photoDict["title"] as? String
                    photo.thumbnailUrl = photoDict["thumbnailUrl"] as? String
                    photo.url = photoDict["url"] as? String
                    photos.append(photo)
                }
                DataBaseManager.instance.saveToDatabase(object: photos)
            }
        }
    }
}


