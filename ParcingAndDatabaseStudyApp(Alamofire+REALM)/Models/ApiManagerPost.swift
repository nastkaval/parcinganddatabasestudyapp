//
//  ApiManagerPosts.swift
//  ParcingAndDatabaseStudyApp(Alamofire+REALM)
//
//  Created by Nastassia  Kavalchuk on 31.05.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import Foundation
import Alamofire

class ApiManagerPost {
    static var instance = ApiManagerPost()
    private enum Constants {
        static let baseURL = "https://jsonplaceholder.typicode.com"
    }
    private enum EndPoints {
        static let posts = "/posts"
    }
    
    func getPosts() {
        let urlString = Constants.baseURL + EndPoints.posts
        Alamofire.request(urlString, method: .get, parameters: [:]).responseJSON { (response) in
            if let arrayPosts = response.result.value as? Array< Dictionary<String,Any> >{
                var posts: [Post] = []
                for postDict in arrayPosts {
                    let post = Post()
                    post.userId = "\(postDict["userId"] as? Int ?? 0)"
                    post.id = "\(postDict["id"] as? Int ?? 0)"
                    post.title = postDict["title"] as? String
                    post.body = postDict["body"] as? String
                    posts.append(post)
                }
                DataBaseManager.instance.saveToDatabase(object: posts)
            }
        }
    }
}
