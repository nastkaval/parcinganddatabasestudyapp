//
//  ApiManagerComment.swift
//  ParcingAndDatabaseStudyApp(Alamofire+REALM)
//
//  Created by Nastassia  Kavalchuk on 04.06.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import Foundation
import Alamofire

class ApiManagerComment {
    static var instance = ApiManagerComment()
    private enum Constants {
        static let baseURL = "https://jsonplaceholder.typicode.com"
    }
    private enum EndPoints {
        static let comments = "/comments"
    }
    
    func getComments() {
        let urlString = Constants.baseURL + EndPoints.comments
        Alamofire.request(urlString, method: .get, parameters: [:]).responseJSON { (response) in
            if let arrayComments = response.result.value as? Array< Dictionary<String,Any> >{
                var comments: [Comment] = []
                for commentDict in arrayComments {
                    let comment = Comment()
                    comment.postId = "\(commentDict["postId"] as? Int ?? 0)"
                    comment.id = "\(commentDict["id"] as? Int ?? 0)"
                    comment.name = commentDict["name"] as? String
                    comment.email = commentDict["email"] as? String
                    comment.body = commentDict["body"] as? String
                    comments.append(comment)
                }
                DataBaseManager.instance.saveToDatabase(object: comments)
            }
        }
    }
}

