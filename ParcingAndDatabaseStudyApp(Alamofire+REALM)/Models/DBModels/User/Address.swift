//
//  RealmAddress.swift
//  TravelApp
//
//  Created by Nastassia  Kavalchuk on 29.05.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class Address: Object {
    @objc dynamic var street: String?
    @objc dynamic var suite: String?
    @objc dynamic var city: String?
    @objc dynamic var zipcode: String?
    @objc dynamic var desc: String?
    @objc dynamic var geo: Geo?
}
