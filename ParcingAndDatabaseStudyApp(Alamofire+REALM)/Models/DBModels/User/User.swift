//
//  RealmUser.swift
//  TravelApp
//
//  Created by Nastassia  Kavalchuk on 29.05.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class User: Object {
    @objc dynamic var name: String?
    @objc dynamic var email: String?
    @objc dynamic var id: String?
    @objc dynamic var username: String?
    @objc dynamic var phone: String?
    @objc dynamic var website: String?
    @objc dynamic var address: Address?
    @objc dynamic var company: Company?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
