//
//  RealmCompany.swift
//  TravelApp
//
//  Created by Nastassia  Kavalchuk on 29.05.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class Company: Object {
    @objc dynamic var name: String?
    @objc dynamic var catchPhrase: String?
    @objc dynamic var bs: String?
}
