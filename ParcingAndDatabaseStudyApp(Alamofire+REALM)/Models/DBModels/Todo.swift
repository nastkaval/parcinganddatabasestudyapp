//
//  Todo.swift
//  ParcingAndDatabaseStudyApp(Alamofire+REALM)
//
//  Created by Nastassia  Kavalchuk on 04.06.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import Foundation
import RealmSwift

class Todo: Object {
    @objc dynamic var userId: String?
    @objc dynamic var id: String?
    @objc dynamic var title: String?
//    @objc dynamic var completed: Bool?
    override static func primaryKey() -> String? {
        return "id"
    }
}
