//
//  Comment.swift
//  ParcingAndDatabaseStudyApp(Alamofire+REALM)
//
//  Created by Nastassia  Kavalchuk on 04.06.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import Foundation
import RealmSwift

class Comment: Object {
    @objc dynamic var postId: String?
    @objc dynamic var id: String?
    @objc dynamic var name: String?
    @objc dynamic var email: String?
    @objc dynamic var body: String?
    override static func primaryKey() -> String? {
        return "id"
    }
}
