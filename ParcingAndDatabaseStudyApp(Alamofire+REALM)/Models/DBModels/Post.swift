//
//  RealmPost.swift
//  ParcingAndDatabaseStudyApp(Alamofire+REALM)
//
//  Created by Nastassia  Kavalchuk on 31.05.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import Foundation
import RealmSwift

class Post: Object {
    @objc dynamic var userId: String?
    @objc dynamic var id: String?
    @objc dynamic var title: String?
    @objc dynamic var body: String?
    override static func primaryKey() -> String? {
        return "id"
    }
}
