//
//  ApiManagerTodo.swift
//  ParcingAndDatabaseStudyApp(Alamofire+REALM)
//
//  Created by Nastassia  Kavalchuk on 04.06.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import Foundation
import Alamofire

class ApiManagerTodo {
    static var instance = ApiManagerTodo()
    private enum Constants {
        static let baseURL = "https://jsonplaceholder.typicode.com"
    }
    private enum EndPoints {
        static let todos = "/todos"
    }
    
    func getTodos () {
        let urlString = Constants.baseURL + EndPoints.todos
        Alamofire.request(urlString, method: .get, parameters: [:]).responseJSON { (response) in
            if let arrayTodos = response.result.value as? Array< Dictionary<String,Any> >{
                var todos: [Todo] = []
                for todoDict in arrayTodos {
                    let todo = Todo()
                    todo.userId = "\(todoDict["userId"] as? Int ?? 0)"
                    todo.id = "\(todoDict["id"] as? Int ?? 0)"
                    todo.title = todoDict["title"] as? String
                    todos.append(todo)
                }
                DataBaseManager.instance.saveToDatabase(object: todos)
            }
        }
    }
}
