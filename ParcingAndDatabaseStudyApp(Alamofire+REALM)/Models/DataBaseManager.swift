//
//  DataBaseManager.swift
//  TravelApp
//
//  Created by Nastassia  Kavalchuk on 29.05.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import Foundation
import RealmSwift

class DataBaseManager {
    
    // MARK: - Properties
    static var instance = DataBaseManager()
    
    // MARK: - Functions
    func saveToDatabase(object: [Object]){
        let realm = try! Realm()
        try! realm.write {
            realm.add(object, update: true)
        }
    }
   func getAllFromDatabase<T: Object>(_ object: T.Type) -> [T]  {
        let realm = try! Realm()
        let result = realm.objects(T.self)
        return Array(result)
    }
}
