//
//  ApiManager.swift
//  TravelApp
//
//  Created by Nastassia  Kavalchuk on 17.05.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//
import Foundation
import Alamofire

class ApiManagerUser {
    
    // MARK: - Properties
    static var instance = ApiManagerUser()
    private enum Constants {
        static let baseURL = "https://jsonplaceholder.typicode.com"
    }
    private enum EndPoints {
        static let users = "/users"
    }
    
    // MARK: - Functions
    func getUsers() {
        let urlString = Constants.baseURL + EndPoints.users
        Alamofire.request(urlString, method: .get, parameters: [:]).responseJSON { (response) in
            if let arrayUsers = response.result.value as? Array< Dictionary<String,Any> >{
                var users: [User] = []
                for userDict in arrayUsers {
                    let user = User()
                    user.id = "\(userDict["id"] as? Int ?? 0)"
                    user.name = userDict["name"] as? String
                    user.phone = userDict["phone"] as? String
                    user.email = userDict["email"] as? String
                    user.username = userDict["username"] as? String
                    user.website = userDict["website"] as? String
                    users.append(user)
                    if let addressDict = userDict["address"] as? Dictionary<String,Any> {
                        let address = Address()
                        address.street = addressDict["street"] as? String ?? ""
                        address.city = addressDict["city"] as? String ?? ""
                        address.suite = addressDict["suite"] as? String ?? ""
                        address.zipcode = addressDict["zipcode"] as? String ?? ""
                        user.address = address
                        if let geoDict = addressDict["geo"] as? Dictionary<String,Any> {
                            let geo = Geo()
                            geo.latitude = geoDict["lat"] as? String ?? ""
                            geo.longitude = geoDict["lng"] as? String ?? ""
                            address.geo = geo
                        }
                    }
                    if let companyDict = userDict["company"] as? Dictionary<String,Any> {
                        let company = Company()
                        company.bs = companyDict["bs"] as? String
                        company.catchPhrase = companyDict["catchPhrase"] as? String
                        company.name = companyDict["name"] as? String
                        user.company = company
                    }
                    DataBaseManager.instance.saveToDatabase(object: users)
                }   
            }
        }
    }
}
